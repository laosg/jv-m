package com.ryan.study.jvm.start;

import java.util.ArrayList;

/**
 * Created by kaimin on 21/2/2019.
 * time : 22:35
 * jconsole 演示
 */
public class JConsoleTest {

    byte[] buffer=new byte[128*1024];//全局变量，运行时新声代老年代的内存图形，和放在构造函数中作为局部变量时不一样的

    public static void main(String[] args) {
      /*  try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/

        System.out.println("start^");
//        fill(1000);
    }

    private static void fill(int n) {
        ArrayList<Cat> list = new ArrayList();
        for (int i=0;i<n;i++) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            list.add(new Cat());
        }
    }
}
