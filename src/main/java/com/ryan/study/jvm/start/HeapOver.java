package com.ryan.study.jvm.start;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kaimin on 21/2/2019.
 * time : 21:48
 *
 * -Xms20M -Xmx20M 指定JVM堆大小,演示堆溢出，因为一只在不停的加对象，所以会堆溢出
 * 要学会，怎么排查
 */
public class HeapOver {
    public static void main(String[] args) {
        List<Cat> cats=new ArrayList<Cat>();
        while (true) {
            cats.add(new Cat());
        }
    }

}
